$(document).ready(function() {
   $("#logForm1").submit(function() {
       submitLogin(1);
       return false;
   });

    $("#logForm2").submit(
        function() {
            submitLogin(2);
            return false;
        }
    );

    function submitLogin(idForm) {
        var login = $("#login" + idForm).val();
        var pass = $("#password" + idForm).val();
        if(login.length < 1) {
            //@TODO навесь красный бордер на инпут
            return;
        }
        if(pass.length < 1) {
            //@TODO навесь красный бордер на инпут
            return;
        }
        $.post("/jauth", {email: login, password: pass}, cleanModals);
    }

    var finalPrice;

    function cleanModals(data) {
        if(data == 'ok') {
            $("#cd-profile").empty();
            $("#cd-profile").append("<a href='/profile'>Мои личные данные</a>");
            $("#cd-profile").append("<a href='/contacts'>Обратная связь</a>");
            $("#cd-profile").append("<a href='/logout'>Выход</a>");

            $("#cd-cart").empty();
            $("#cd-cart").append('<h2>Корзина</h2>');
            $('<ul id="itms"></ul>').addClass("cd-cart-items").appendTo("#cd-cart");
            finalPrice = 0;
            $.post("/basket/show", showItems);

        } else {
            $("#error1").html("<h4>Error</h4>");
            $("#error2").html("<h4>Error</h4>");

            $("login1").val("");
            $("login2").val("");
            $("password1").val("");
            $("password2").val("");

        }
    }

    function showItems(data) {
        $.each(data, function() {
            $("#itms").append("<li>");
            $("<span>", {"class":"cd-qty"}).html(this.count + "x").appendTo("#itms");
            $("#itms").append(this.name);
            //$("#itms").append("<span class='cd-qty'>" + this.count + "x</span> " + this.name);
            $("<div></div>").addClass("cd-price").html("Размер: " + this.size + ", " + this.price + " руб.").appendTo("#itms");
            //$("#itms").append("<div class='cd-price'>Размер: " + this.size + ", " + this.price + " руб.</div>");
            //$("<a href='#'></a>").addClass("cd-item-remove").addClass(" cd-img-replace").html("remove").appendTo("#itms");
            $("#itms").append("</li>");
            finalPrice = parseInt(this.price)*parseInt(this.count) + finalPrice;
        });
        if(finalPrice != 0) {
            $("#cd-cart").append("<div class='cd-cart-total'>");
            $("#cd-cart").append("<p>Общая сумма: <span id='finalPrice'>" + finalPrice + " руб.</span></p>");
            $("#cd-cart").append("</div>");
            $("#cd-cart").append("<a href='/basket/checkout' class='cd-go-to-cart' href='#0'>Перейти к оформлению заказа</a>");
        } else {
            $("#cd-cart").append("<p>Товаров в корзине нету</p>");
        }
    }

    $(".cd-item-remove").click(function() {
        $(this).closest('li').remove();
        $.post("/basket/del", {id: $(this).closest('li').attr("id")});
    })
});

