$(document).ready( function() {
    $(".button").click(function() {
        if($(this).hasClass("empty")) return;
        $(".button").removeClass("pressed");
        $(this).addClass("pressed");
    });

    $(".in-cart").click(function() {
        var size = $(".pressed").html();
        if(size == null) {
            alert("Выберите размер для добавления в корзину");
            return;
        }
        $.post("/basket/add", {id: $("#itemId").val(), count: $("#count").val(), size: size}, function(data) {
            $("#itms").append("<li>");
            $("<span></span>").addClass("cd-qty").html(data.count + "x").appendTo("#itms");
            $("#itms").append(data.name);
            $("<div></div>").addClass("cd-price").html("Размер: " + data.size + ", " + data.price + " руб.").appendTo("#itms");
            $("#itms").append("</li>");
            //$(".cd-cart-items").append("<span class='cd-qty'>" + this.count + "x</span> " + this.name);
            //$(".cd-cart-items").append("<div class='cd-price'>Размер: " + this.size + ", " + this.price + " руб.</div>");
            //$(".cd-cart-items").append("<a href='#' >Remove</a>");
            //$(".cd-cart-items").append("</li>");
            $("#finalPrice").html(parseInt($("#finalPrice").html()) + data.price*data.count);
        })
    });

});
