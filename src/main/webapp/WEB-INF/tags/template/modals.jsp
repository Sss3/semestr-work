﻿<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="clear"></div>

<div id="cd-shadow-layer"></div>

<div id="cd-profile">
    <security:authorize access="isAuthenticated()">
        <a href="<c:url value="/profile"/>">Мой профиль</a>
        <a href="<c:url value="/contacts"/>">Обратная связь</a>
        <a href="<c:url value="/logout"/>">Выход</a>
    </security:authorize>
    <security:authorize access="isAnonymous()">
        <span class="noreg"><a href="<c:url value="/auth" />">Нет профиля в нашем магазине?</a></span>
        <div id="error1"></div>

        <form id="logForm1">
            <span class="info">Логин</span>
        <input type="text" id="login1">
        <br/>
            <span class="info">Пароль</span>
        <input type="password" id="password1" >
        <br/>
        <input id="auth1" value="Вход" type="submit">
        </form>
    </security:authorize>
</div>

<div id="cd-cart">
    <security:authorize access="isAuthenticated()">
        <h2>Корзина</h2><ul class="cd-cart-items" id="itms">
        <c:if test="${fn:length(basket) > 0}">

            <c:forEach items="${basket}" var="product">
                <li id="${product.id}">
                    <span class="cd-qty">${product.count}x</span> ${product.item.name}
                    <div class="cd-price">Размер: ${product.size}, ${product.item.price} руб.</div>
                    <a href='#' class="cd-item-remove cd-img-replace">Remove</a>
                </li>
            </c:forEach>
            </ul>


        <!-- cd-cart-items -->

        <div class="cd-cart-total">
            <p>Общая сумма: <span>${totalPrice} руб.</span></p>
        </div>
            <a class="cd-go-to-cart" href="<c:url value="/basket/checkout"/>">Перейти к оформлению заказа</a>
        </c:if>
    <%--</ul>--%>
        <c:if test="${fn:length(basket) == 0}">
            <p>Товаров в корзине нету</p>
        </c:if>
        <!-- cd-cart-total -->

    </security:authorize>
    <security:authorize access="isAnonymous()">
        <span class="noreg"><a href="<c:url value="/auth" />">Нет профиля в нашем магазине?</a></span>
        <div id="error2"></div>

        <form id="logForm2">
            <span class="info">Логин</span>
        <input type="text" id="login2" >
        <br/>
            <span class="info">Пароль</span>
        <input type="password" id="password2">
        <br/>
            <input id="auth2" value="Вход" type="submit">
        </form>
    </security:authorize>
</div>
<!-- cd-cart -->