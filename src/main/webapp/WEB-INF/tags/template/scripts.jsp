<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/js/jquery-1.11.2.min.js"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/js/main.js"/>"></script> <!-- Gem jQuery -->
<script src="<c:url value="/resources/js/profile.js"/>"></script>
<script src="<c:url value="/resources/js/login.js"/>"></script>