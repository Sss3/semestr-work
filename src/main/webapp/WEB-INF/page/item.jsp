<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<t:template title="${item.name}">

    <jsp:attribute name="body">
        <main>
            <div class="wrap">
                <div id="breadcrumbs">
                    <a href="<c:url value="/" />">Главная</a> /
                    <a href="<c:url value="/${item.mainCategory.name}" />">${item.mainCategory.alias}</a> /
                    <a href="<c:url value="/${item.subCategory.name}" />">${item.subCategory.alias}</a> /
                    <a href="#" class="active">${item.name}</a>

                </div>

                <div id="cart">

                    <div class="img-slide">
                        <img src="<c:url value="${item.photo}" />" class="img-responsive">
                    </div>

                    <div class="description">

                        <h1>${item.name}</h1>

                        <input type="hidden" value="${item.id}" id="itemId"/>

                        <span class="price">${item.price}руб.</span>

                        <div class="size">
                            <span>Размер:</span>
                            <%--<a href="#">таблица размеров</a>--%>
                            <div class="clear"></div>
                            <c:set var="pr" value="${item}"/>
                            <c:forEach items="${pr.itemSizes}" var="size">
                                <span class="button <c:if test="${size.count <= 0}">empty</c:if>">${size.size}</span>
                            </c:forEach>
                        </div>

                        <div class="count">
                            <span>Количество:</span>
                            <input type="number" value="1" id="count">
                        </div>
                        <security:authorize access="isAuthenticated()">
                        <a href="#" class="in-cart">В корзину</a>
                        </security:authorize>
                        <security:authorize access="isAnonymous()">
                            <a href="<c:url value="/auth" />" class="in-cart-no-auth">Авторизироваться</a>
                                </security:authorize>

                    </div>
                </div>

            </div>
        </main>

    </jsp:attribute>

    <jsp:attribute name="scripts">
        <script type="text/javascript" src="<c:url value="/resources/js/items.js" />"></script>
    </jsp:attribute>


</t:template>