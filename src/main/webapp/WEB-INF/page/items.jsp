<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:template title="Items">

    <jsp:attribute name="body">


        <aside>

            <form method="get">

                <div class="category">
                    <a>Категории</a>
                    <ul>
                        <ul class="subcategory">
                            <a href="#">Мужчинам</a>
                            <li><a href="<c:url value="/items/man/form" />">Игровые футболки</a></li>
                            <li><a href="<c:url value="/items/man/shorts" />">Игровые шорты</a></li>
                            <li><a href="<c:url value="/items/man/jacket" />">Куртки/Ветровки</a></li>
                            <li><a href="<c:url value="/items/man/retro" />">Ретро формы</a></li>
                        </ul>
                        <ul class="subcategory">
                            <a href="#">Женщинам</a>
                            <li><a href="<c:url value="/items/women/form" />">Игровые футболки</a></li>
                            <li><a href="<c:url value="/items/women/shorts" />">Игровые шорты</a></li>
                            <li><a href="<c:url value="/items/women/jacket" />">Куртки/Ветровки</a></li>
                            <li><a href="<c:url value="/items/women/retro" />">Ретро формы</a></li>
                        </ul>
                        <ul class="subcategory">
                            <a href="#">Детям</a>
                            <li><a href="<c:url value="/items/child/form" />">Игровые футболки</a></li>
                            <li><a href="<c:url value="/items/child/shorts" />">Игровые шорты</a></li>
                            <li><a href="<c:url value="/items/child/jacket" />">Куртки/Ветровки</a></li>
                            <li><a href="<c:url value="/items/child/retro" />">Ретро формы</a></li>
                        </ul>
                    </ul>

                </div>

                <div class="price">
                    <a href="#">Цена (Руб.)</a>

                    <div class="clear"></div>
                    <div class="to">
                        <span>От:</span>

                        <div class="clear"></div>
                        <input type="number" name="minPrice" value="${param.minPrice}">
                    </div>
                    <div class="to">
                        <span>До:</span>

                        <div class="clear"></div>
                        <input type="number" name="maxPrice" value="${param.maxPrice}">
                    </div>

                </div>

                <div class="size">
                    <a href="#">Размер</a>

                    <div class="clear"></div>
                    <div class="size-wrap">
                        <div class="check-block">
                            <input type="checkbox" name="sizeS" <c:if test="${not empty param.sizeS}"> checked </c:if> >S
                        </div>
                        <div class="check-block">
                            <input type="checkbox" name="sizeM" <c:if test="${not empty param.sizeM}"> checked </c:if> >M
                        </div>
                        <div class="check-block">
                            <input type="checkbox" name="sizeL" <c:if test="${not empty param.sizeL}"> checked </c:if> >L
                        </div>
                        <div class="check-block">
                            <input type="checkbox" name="sizeXL" <c:if test="${not empty param.sizeXL}"> checked </c:if> >XL
                        </div>
                        <div class="check-block">
                            <input type="checkbox" name="sizeXXL" <c:if test="${not empty param.sizeXXL}"> checked </c:if> >XXL
                        </div>
                        <input type="submit" value="Показать">

                        <div class="clear"></div>
                        <a class="reset" href="<c:url value="/items" />">Сбросить все</a>

                    </div>


                </div>

                <div class="color">

                </div>

            </form>

        </aside>

        <main class="products-page">

            <div id="products-pagination">
                <span>Показаны ${pageSize} результатов из ${allSize}</span>

                <div id="pagi">
                    <span>Товаров на странице:</span>

                    <c:if test="${param.size != 9 && not empty param.size}"> <div class="number"> <a href="<c:url value="${currentURL}&page=0&size=9"/>" >9</a></div> </c:if>
                    <c:if test="${empty param.size || param.size == 9}"><div class="number-active">9</div> </c:if><%--@TODO выдели как нить тип "выбрано"--%>

                    <c:if test="${param.size != 18}"> <div class="number"> <a href="<c:url value="${currentURL}&page=0&size=18"/>" >18</a></div> </c:if>
                    <c:if test="${param.size == 18}"> <div class="number-active"> 18 </div></c:if>

                    <c:if test="${param.size != 2000}"> <div class="number"> <a href="<c:url value="${currentURL}&page=0&size=2000"/>" >Все</a></div> </c:if>
                    <c:if test="${param.size == 2000}"> <div class="number-active">Все </div></c:if>
                </div>
            </div>

            <div id="product-list">
                <p class="clear">
                    <c:forEach items="${products}" var="product" varStatus="index">
                <c:if test="${index.count%4==0}">
                    <p class="clear"></p>
                </c:if>


                <div class="product-wrap" id="product${product.id}">
                    <div class="product">
                        <c:if test="${not empty product.discount && product.discount > 0}"><span
                                class="discount">${product.discount}%</span></c:if>

                        <div class="product-img">
                            <a href="<c:url value="/item/${product.id}" />"><img
                                    src="<c:url value="${product.photo}" />" alt="${product.name}" class="img-responsive product-img-size"/></a>
                        </div>
                        <p class="product_name">${product.name}</p>

                        <div class="action_block">
                            <div class="price">${product.price} руб</div>
                            <div class="size">S</div>
                            <a class="basket" href="<c:url value="/item/${product.id}" />">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="svg-basket">
                                    <g id="shopping-cart">
                                        <path d="M7,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S8.1,18,7,18z M1,2v2h2l3.6,7.6L5.2,14C5.1,14.3,5,14.7,5,15c0,1.1,0.9,2,2,2h12v-2H7.4c-0.1,0-0.2-0.1-0.2-0.2c0,0,0-0.1,0-0.1L8.1,13h7.4c0.8,0,1.4-0.4,1.7-1l3.6-6.5C21,5.3,21,5.2,21,5c0-0.6-0.4-1-1-1H5.2L4.3,2H1z M17,18c-1.1,0-2,0.9-2,2s0.9,2,2,2c1.1,0,2-0.9,2-2S18.1,18,17,18z"></path>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>

                </c:forEach>
            </div>


        </main>


    </jsp:attribute>


</t:template>