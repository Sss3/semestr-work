<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:template title="Auth">

    <jsp:attribute name="body">
        <main>
            <h2>Auth</h2>
            <form action="<c:url value="/auth"/>" method="POST">
                <input type="text" placeholder="Email" name="email" autofocus/>
                <br/>
                <input type="password" name="pass" placeholder="Password"/>
                <br/>

                <button type="submit" class="btn btn-default">Login</button>
            </form>
            <hr/>
            <h2>Регистрация</h2><form action="<c:url value="/reg" />" method="POST">
                <input type="text" placeholder="Email" name="email" />
                <br/>
                <input type="password" name="pass" placeholder="Password"/>
                <br/>
                <button type="submit" class="btn btn-default">Регистрация</button>
            </form>
        </main>
    </jsp:attribute>


</t:template>