<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:template title="New ITEM">

    <jsp:attribute name="body">
        <main>
        <h3>Новый предмет</h3>

        <form:form modelAttribute="itemForm" method="post" ENCTYPE="multipart/form-data">

            <form:label path="name">Название: </form:label>
            <form:input path="name" type="text"/>
            <form:errors path="name"/>

            <form:label path="price">Цена: </form:label>
            <form:input path="price" type="number"/>
            <form:errors path="price"/>

            <form:label path="discount">Скидка: </form:label>
            <form:input path="discount" type="number"/>

            <form:label path="sizeS">Количество (размер S) : </form:label>
            <form:input path="sizeS" type="number"/>

            <form:label path="sizeM">Количество (размер M) : </form:label>
            <form:input path="sizeM" type="number"/>

            <form:label path="sizeL">Количество (размер L) : </form:label>
            <form:input path="sizeL" type="number"/>

            <form:label path="sizeXL">Количество (размер XL) : </form:label>
            <form:input path="sizeXL" type="number"/>

            <form:label path="sizeXXL">Количество (размер XXL) : </form:label>
            <form:input path="sizeXXL" type="number"/>

            <form:label path="mainCategory">Основная категория: </form:label>
            <form:select path="mainCategory" size="1">
                <form:options items="${mainCategory}" itemLabel="name" itemValue="id"/>
            </form:select>

            <form:label path="subCategory">Подкатегория: </form:label>
            <form:select path="subCategory" size="1">
                <form:options items="${subCategory}" itemLabel="name" itemValue="id"/>
            </form:select>

            <form:label path="photo">Фото: </form:label>
            <form:input path="photo" type="file"/>
            <form:errors path="photo"/>

            <form:hidden path="id"/>

            <input type="submit" value="add"/>
        </form:form>
        </main>
    </jsp:attribute>

</t:template>