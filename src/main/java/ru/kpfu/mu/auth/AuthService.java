package ru.kpfu.mu.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.kpfu.mu.model.User;
import ru.kpfu.mu.service.UserService;

/**
 * @author Alexeev Vladimir  06.05.2015
 */
@Service
public class AuthService implements UserDetailsService{

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userService.getByEmail(s);
        if(user == null) throw new UsernameNotFoundException("");
        return new AuthUser(user);
    }
}
