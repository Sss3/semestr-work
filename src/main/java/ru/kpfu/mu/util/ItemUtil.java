package ru.kpfu.mu.util;

public class ItemUtil {


    public static String renderCurrentUrl(String category, String subcategory, Float minPrice, Float maxPrice,
                                    Boolean sizeS, Boolean sizeM, Boolean sizeL, Boolean sizeXL, Boolean sizeXXL) {
        StringBuilder sb = new StringBuilder();
        sb.append("/items/");
        if(category != null && !category.isEmpty()) sb.append(category).append("/");
        if(subcategory != null && !category.isEmpty()) sb.append(subcategory);
        sb.append("?");
        if(maxPrice != null) sb.append("maxPrice=").append(maxPrice).append("&");
        if(minPrice != null) sb.append("minPrice=").append(minPrice).append("&");
        if(sizeS != null && sizeS) sb.append("sizeS=").append("on").append("&");
        if(sizeM != null && sizeM) sb.append("sizeM=").append("on").append("&");
        if(sizeL != null && sizeL) sb.append("sizeL=").append("on").append("&");
        if(sizeXL != null && sizeXL) sb.append("sizeXL=").append("on").append("&");
        if(sizeXXL != null && sizeXXL) sb.append("sizeXXL=").append("on").append("&");
        return sb.toString();
    }

}
