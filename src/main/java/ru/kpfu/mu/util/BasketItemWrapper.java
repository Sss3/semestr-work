package ru.kpfu.mu.util;

import ru.kpfu.mu.model.BasketItem;
import ru.kpfu.mu.model.Size;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BasketItemWrapper implements Serializable{

    private static final long serialVersionUID = 2116833901235267577L;
    private Long id;
    private Integer count;
    private String name;
    private Float price;
    private Size size;

    private BasketItemWrapper() {}

    public BasketItemWrapper(BasketItem item) {
        this.id = id;
        this.count = item.getCount();
        this.name = item.getItem().getName();
        this.price = item.getItem().getPrice();
        this.size = item.getSize();
    }

    public static List<BasketItemWrapper> getWrappers(List<BasketItem> items) {
        List<BasketItemWrapper> result = new ArrayList<>(items.size());
        items.forEach(item -> result.add(new BasketItemWrapper(item)));
        return result;
    }

    public Integer getCount() {
        return count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}
