package ru.kpfu.mu.service;

import ru.kpfu.mu.model.BasketItem;
import ru.kpfu.mu.model.User;

import java.util.List;

public interface BasketService {

    BasketItem save(BasketItem item);

    List<BasketItem> getUserItems(User user);

    void removeAllItemsUser(User user);

    void delete(Long id);

}
