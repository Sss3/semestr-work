package ru.kpfu.mu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.kpfu.mu.model.Category;
import ru.kpfu.mu.model.Item;
import ru.kpfu.mu.model.ItemSize;
import ru.kpfu.mu.model.Size;
import ru.kpfu.mu.repository.CategoryRepository;
import ru.kpfu.mu.repository.ItemRepository;
import ru.kpfu.mu.repository.ItemSizeRepository;
import ru.kpfu.mu.service.ItemService;

import java.util.*;

@Service
public class ItemServiceImpl implements ItemService {

    private static final String SORT_BY_DATE = "date";
    private static final String SORT_BY_PRICE = "price";

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ItemSizeRepository itemSizeRepository;

    @Override
    public Page<Item> getPageItems(String category, String subcategory, Float minPrice, Float maxPrice,
                                   Boolean sizeS, Boolean sizeM, Boolean sizeL, Boolean sizeXL, Boolean sizeXXL,
                                   Integer page, Integer pageSize) {

        if(minPrice == null) minPrice = 0F;
        if(maxPrice == null) maxPrice = Float.MAX_VALUE;
        if(sizeS == null) sizeS = false;
        if(sizeM == null) sizeM = false;
        if(sizeL == null) sizeL = false;
        if(sizeXL == null) sizeXL = false;
        if(sizeXXL == null) sizeXXL = false;
        Pageable pageable = new PageRequest(page, pageSize);
        Category mainCategory = categoryRepository.findByName(category);
        if(mainCategory == null || category.isEmpty()) return itemRepository.findAll(pageable);
        Category subCategory = categoryRepository.findByName(subcategory);

        Collection<Size> sizes = getSizes(sizeS, sizeM, sizeL, sizeXL, sizeXXL);

        if(subCategory == null || subcategory.isEmpty()) return itemRepository
                .findToPage(mainCategory, minPrice, maxPrice, sizes, pageable);

        return itemRepository.findToPage(mainCategory, subCategory, minPrice, maxPrice, sizes, pageable);
    }

    @Override
    public Item getById(Long id) {
        return itemRepository.findOne(id);
    }

    @Override
    public Set<ItemSize> save(Set<ItemSize> itemSizes) {
        Set<ItemSize> set = new HashSet<>();
        itemSizeRepository.save(itemSizes).forEach((itemSize) -> set.add(itemSize));
        return set;
    }

    @Override
    public Item save(Item item) {
        return itemRepository.save(item);
    }

    @Override
    public List<Item> getPopular() {
        return itemRepository.findAll(new PageRequest(0, 3, new Sort(Sort.Direction.DESC, "rate"))).getContent();
    }

    @Override
    public List<Item> getNew() {
        return itemRepository.findAll(new PageRequest(0, 3, new Sort(Sort.Direction.DESC, "id"))).getContent();
    }

    private Collection<Size> getSizes(Boolean sizeS, Boolean sizeM, Boolean sizeL, Boolean sizeXL, Boolean sizeXXL) {
        Collection<Size> sizes = new ArrayList<>();
        if(sizeS) sizes.add(Size.S);
        if(sizeM) sizes.add(Size.M);
        if(sizeL) sizes.add(Size.L);
        if(sizeXL) sizes.add(Size.XL);
        if(sizeXXL) sizes.add(Size.XXL);
        if(sizes.size() == 0) {
            sizes.addAll(Arrays.asList(Size.S, Size.M, Size.L, Size.XL, Size.XXL));
        }
        return sizes;
    }

}
