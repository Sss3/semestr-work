package ru.kpfu.mu.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.mu.model.BasketItem;
import ru.kpfu.mu.model.User;
import ru.kpfu.mu.repository.BasketItemRepository;
import ru.kpfu.mu.service.BasketService;

import java.util.List;

@Service
public class BasketServiceImpl implements BasketService {

    @Autowired
    private BasketItemRepository repository;

    @Override
    public BasketItem save(BasketItem item) {
        return repository.save(item);
    }

    @Override
    public List<BasketItem> getUserItems(User user) {
        return repository.findByUser(user);
    }

    @Override
    public void removeAllItemsUser(User user) {
//        repository.deleteByUser(user);
        repository.delete(repository.findByUser(user));
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }
}
