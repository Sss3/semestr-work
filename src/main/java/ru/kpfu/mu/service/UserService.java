package ru.kpfu.mu.service;

import ru.kpfu.mu.model.User;

/**
 * @author Alexeev Vladimir  06.05.2015
 */
public interface UserService {

    User getByEmail(String email);

    User save(User user);

    User getById(Long id);

}
