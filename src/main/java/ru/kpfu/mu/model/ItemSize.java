package ru.kpfu.mu.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class ItemSize implements Serializable{

    private static final long serialVersionUID = -4037259285300104063L;
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    @JoinColumn
    private Item item;

    @Enumerated
    private Size size;

    private Integer count;

    public ItemSize() {

    }

    public ItemSize(Long id) {
        this.id = id;
    }

    public ItemSize(Item item, Integer count, Size size) {
        this.item = item;
        this.count = count;
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Integer getCount() {
        if(count == null) return 0;
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}
