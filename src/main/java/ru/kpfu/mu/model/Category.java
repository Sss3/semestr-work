package ru.kpfu.mu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Category implements Serializable{

    private static final long serialVersionUID = 8722397178326628150L;
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;
    private Boolean main;
    private String alias;

    public Category() {}

    public Category(Long id) {
        this.id = id;
    }

    public Category(String name, Boolean main, String alias) {
        this.name = name;
        this.main = main;
        this.alias = alias;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getMain() {
        return main != null && main;
    }

    public void setMain(Boolean main) {
        this.main = main;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
