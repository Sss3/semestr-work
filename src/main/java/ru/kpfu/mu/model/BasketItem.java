package ru.kpfu.mu.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class BasketItem implements Serializable{

    private static final long serialVersionUID = -8470768260271502851L;
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Item item;

    @OneToOne
    @JoinColumn(nullable = false)
    private User user;

    private Integer count;

    @Enumerated
    private Size size;

    private BasketItem(){}

    private BasketItem(Long id) {
        this.id = id;
    }

    public BasketItem(Item item, User user, Integer count, Size size) {
        this.item = item;
        this.user = user;
        this.count = count;
        this.size = size;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}
