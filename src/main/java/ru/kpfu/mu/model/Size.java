package ru.kpfu.mu.model;

public enum Size {
    S(0), M(1), L(2), XL(3), XXL(4);

    private int id;
    private Size(int id) {
        this.id = id;
    }
}
