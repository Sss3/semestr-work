package ru.kpfu.mu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.mu.model.BasketItem;
import ru.kpfu.mu.model.Item;
import ru.kpfu.mu.service.BasketService;
import ru.kpfu.mu.service.ItemService;
import ru.kpfu.mu.util.AuthUtil;
import ru.kpfu.mu.util.ItemUtil;

import java.util.List;

@Controller
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private BasketService basketService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String mainPage(ModelMap map) {
        map.put("newProduct", itemService.getNew());
        map.put("popular", itemService.getPopular());
        if(AuthUtil.isAuthenticated()) {
            List<BasketItem> basketItems = basketService.getUserItems(AuthUtil.getCurrentUser().getUser());
            map.put("basket", basketItems);
            final int[] totalPrice = {0};
            basketItems.forEach((item) -> totalPrice[0] += item.getItem().getPrice()*item.getCount());
            map.put("totalPrice", totalPrice[0]);
        }
        return "home";
    }

    @RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
    public String itemPage(@PathVariable Long id, ModelMap map) {
        map.put("item", itemService.getById(id));
        if(AuthUtil.isAuthenticated()) {
            List<BasketItem> basketItems = basketService.getUserItems(AuthUtil.getCurrentUser().getUser());
            map.put("basket", basketItems);
            final int[] totalPrice = {0};
            basketItems.forEach((item) -> totalPrice[0] += item.getItem().getPrice()*item.getCount());
            map.put("totalPrice", totalPrice[0]);
        }
        return "item";
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String itemsPageWithOutMainAndSubCategory(@RequestParam(required = false) Float minPrice,
                                                     @RequestParam(required = false) Float maxPrice,
                                                     @RequestParam(required = false) Boolean sizeS,
                                                     @RequestParam(required = false) Boolean sizeM,
                                                     @RequestParam(required = false) Boolean sizeL,
                                                     @RequestParam(required = false) Boolean sizeXL,
                                                     @RequestParam(required = false) Boolean sizeXXL,
                                                     @RequestParam(defaultValue = "0") Integer page,
                                                     @RequestParam(defaultValue = "9") Integer pageSize,

                                                     ModelMap map) {
        return itemsPage(null, null, minPrice, maxPrice, sizeS, sizeM, sizeL, sizeXL, sizeXXL, page, pageSize, map);
    }

    @RequestMapping(value = "/items/{mainCategory}", method = RequestMethod.GET)
    public String itemsPageWithOutSubCategory(@PathVariable("mainCategory") String category,
                                                     @RequestParam(required = false) Float minPrice,
                                                     @RequestParam(required = false) Float maxPrice,
                                                     @RequestParam(required = false) Boolean sizeS,
                                                     @RequestParam(required = false) Boolean sizeM,
                                                     @RequestParam(required = false) Boolean sizeL,
                                                     @RequestParam(required = false) Boolean sizeXL,
                                                     @RequestParam(required = false) Boolean sizeXXL,
                                                     @RequestParam(defaultValue = "0") Integer page,
                                                     @RequestParam(defaultValue = "9") Integer pageSize,

                                                     ModelMap map) {
        return itemsPage(category, null, minPrice, maxPrice, sizeS, sizeM, sizeL, sizeXL, sizeXXL, page, pageSize, map);
    }


    @RequestMapping(value = "/items/{mainCategory}/{subCategory}", method = RequestMethod.GET)
    public String itemsPage(@PathVariable("mainCategory") String category,
                            @PathVariable("subCategory")String subcategory,
                            @RequestParam(required = false) Float minPrice,
                            @RequestParam(required = false) Float maxPrice,
                            @RequestParam(required = false) Boolean sizeS,
                            @RequestParam(required = false) Boolean sizeM,
                            @RequestParam(required = false) Boolean sizeL,
                            @RequestParam(required = false) Boolean sizeXL,
                            @RequestParam(required = false) Boolean sizeXXL,
                            @RequestParam(defaultValue = "0") Integer page,
                            @RequestParam(defaultValue = "9") Integer pageSize,

                            ModelMap map) {

        Page<Item> items = itemService.getPageItems(category, subcategory, minPrice, maxPrice, sizeS, sizeM, sizeL, sizeXL, sizeXXL, page, pageSize);
        map.put("products", items.getContent());
        map.put("pageSize", (items.getSize() < items.getTotalPages())? items.getSize() : items.getTotalElements());
        map.put("allSize", items.getTotalElements());
        map.put("currentURL", ItemUtil.renderCurrentUrl(category, subcategory, minPrice, maxPrice, sizeS, sizeM, sizeL, sizeXL, sizeXXL));
        if(AuthUtil.isAuthenticated()) {
            List<BasketItem> basketItems = basketService.getUserItems(AuthUtil.getCurrentUser().getUser());
            map.put("basket", basketItems);
            final int[] totalPrice = {0};
            basketItems.forEach((item) -> totalPrice[0] += item.getItem().getPrice()*item.getCount());
            map.put("totalPrice", totalPrice[0]);
        }
        return "items";
    }



}
