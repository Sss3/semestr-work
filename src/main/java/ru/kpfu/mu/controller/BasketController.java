package ru.kpfu.mu.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.mu.model.BasketItem;
import ru.kpfu.mu.model.Item;
import ru.kpfu.mu.model.Size;
import ru.kpfu.mu.model.User;
import ru.kpfu.mu.service.BasketService;
import ru.kpfu.mu.service.ItemService;
import ru.kpfu.mu.util.AuthUtil;
import ru.kpfu.mu.util.BasketItemWrapper;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping(value = "/basket")
@PreAuthorize("hasRole('USER')")
public class BasketController {

    @Autowired
    private BasketService basketService;

    @Autowired
    private ItemService itemService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody BasketItemWrapper addItem(Long id, Integer count, String size) {
        BasketItem item = new BasketItem(itemService.getById(id), AuthUtil.getCurrentUser().getUser(), count, Size.valueOf(size));
        return new BasketItemWrapper(basketService.save(item));
    }

    @RequestMapping(value = "/show", method = RequestMethod.POST)
    public @ResponseBody List<BasketItemWrapper>  showItems() {
        return BasketItemWrapper.getWrappers(basketService.getUserItems(AuthUtil.getCurrentUser().getUser()));
    }

    @RequestMapping(value = "/checkout", method = RequestMethod.GET)
    public @ResponseBody String checkOut() {
        basketService.removeAllItemsUser(AuthUtil.getCurrentUser().getUser());
        return "Заказ оформлен";
    }

    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public @ResponseBody String deleteItem(Long id) {
        System.out.println(id);
        basketService.delete(id);
        return "ok";
    }

}
