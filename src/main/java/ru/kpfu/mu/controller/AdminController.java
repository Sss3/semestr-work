package ru.kpfu.mu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.mu.controller.form.ItemForm;
import ru.kpfu.mu.excepetion.Exception404;
import ru.kpfu.mu.model.Item;
import ru.kpfu.mu.repository.CategoryRepository;
import ru.kpfu.mu.service.ItemService;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

@Controller
@RequestMapping(value = "/admin")
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ItemService itemService;

    @Resource
    private Environment env;

    @RequestMapping(value = {"/newItem", "/update"}, method = RequestMethod.GET)
    public String renderNewItemPage(@RequestParam(required = false) Long id, ModelMap map) {
        map.put("mainCategory", categoryRepository.findByMainTrue());
        map.put("subCategory", categoryRepository.findByMainFalse());
        Item item = (id == null) ? null : itemService.getById(id);
        if(id != null && item == null ) throw new Exception404("Item not found");
        ItemForm form = (item == null)? new ItemForm() : new ItemForm(item);
        map.put("itemForm", form);
        return "admin/newItem";
    }

    @RequestMapping(value = {"/newItem", "/update"}, method = RequestMethod.POST)
    public String updateItem(@Valid ItemForm form, BindingResult result, ModelMap map) {
        if(result.hasErrors()) {
            map.put("mainCategory", categoryRepository.findByMainTrue());
            map.put("subCategory", categoryRepository.findByMainFalse());
            return "admin/newItem";
        }
        Item item = form.getInstance();
        item.setPhoto("/img/" + savePhoto(form.getPhoto()));
        itemService.save(item);
        itemService.save(item.getItemSizes());
        return renderNewItemPage(null, map);
    }

    private String savePhoto(MultipartFile multipartFile) {

        String folder = env.getRequiredProperty("download.folder").substring(5);
        File file;
        String fileName = generatePhotoName(7);
        while( (file = new File(folder + fileName)).exists()) {
            fileName = generatePhotoName(7);
        }
        try(BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            byte[] bytes = multipartFile.getBytes();
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            return null;
        }
        return fileName;
    }

    private String generatePhotoName(int len) {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder sb = new StringBuilder(len);
        Random random = new Random();
        for(int i = 0; i < len; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        sb.append(".jpg");
        return sb.toString();
    }


}
