package ru.kpfu.mu.controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.kpfu.mu.excepetion.Exception404;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(Exception404.class)
    public String error404(Exception exception, ModelMap map) {
        map.put("message", exception.getMessage());
        return "exception";
    }
}
