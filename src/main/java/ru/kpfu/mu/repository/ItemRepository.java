package ru.kpfu.mu.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kpfu.mu.model.Category;
import ru.kpfu.mu.model.Item;
import ru.kpfu.mu.model.Size;

import java.util.Collection;
import java.util.List;

@Repository
public interface ItemRepository extends PagingAndSortingRepository<Item, Long> {

    @Query("SELECT DISTINCT i FROM Item AS i WHERE i.mainCategory = ?1 AND i.subCategory = ?2 " +
            "AND i.price BETWEEN ?3 AND ?4 AND EXISTS(SELECT its FROM ItemSize as its where its.size IN ?5 and its.count > 0)")
    Page<Item> findToPage(Category mainCategory, Category subCategory, Float minPrice, Float maxPrice,
                                     Collection<Size> sizes, Pageable page);

    @Query("SELECT DISTINCT i FROM Item AS i WHERE i.mainCategory = ?1 " +
            "AND i.price BETWEEN ?2 AND ?3 AND EXISTS(SELECT its FROM ItemSize as its where its.size IN ?4 and its.count > 0)")
    Page<Item> findToPage(Category mainCategory, Float minPrice, Float maxPrice,
                          Collection<Size> sizes, Pageable page);

}
