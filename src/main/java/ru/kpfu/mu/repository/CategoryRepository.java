package ru.kpfu.mu.repository;

import org.springframework.data.repository.CrudRepository;
import ru.kpfu.mu.model.Category;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, Long> {

    Category findByName(String name);

    List<Category> findByMainTrue();

    List<Category> findByMainFalse();

}
