package ru.kpfu.mu.repository;

import org.springframework.data.repository.CrudRepository;
import ru.kpfu.mu.model.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findByName(String name);
}
