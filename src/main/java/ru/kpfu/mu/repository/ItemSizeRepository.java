package ru.kpfu.mu.repository;

import org.springframework.data.repository.CrudRepository;
import ru.kpfu.mu.model.ItemSize;

public interface ItemSizeRepository extends CrudRepository<ItemSize, Long> {
}
