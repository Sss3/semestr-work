package ru.kpfu.mu.repository;

import org.springframework.data.repository.CrudRepository;
import ru.kpfu.mu.model.BasketItem;
import ru.kpfu.mu.model.User;

import java.util.List;

public interface BasketItemRepository extends CrudRepository<BasketItem, Long> {

    List<BasketItem> findByUser(User user);

    void deleteByUser(User user);

}
